/*
 * cs_helper.cpp

 *
 *  Created on: Aug 19, 2014
 *      Author: dsykes
 */

#include "AccountMgr.h"
#include "Chat.h"
#include "Language.h"
#include "Player.h"
#include "ScriptMgr.h"

enum HELPERS
{
	HELPER = 11325
};

class cs_helper : public CommandScript
{
public:
	cs_helper() : CommandScript("cs_helper") { }

	ChatCommand* GetCommands() const override
	{
		static ChatCommand commandTable[] =
		{
				{ "helper",			rbac::RBAC_PERM_COMMAND_HELP,			false,	&HandleHelperCommand,			"", NULL },
				{ NULL,			0,			false,	NULL,			"", NULL }
		};

		return commandTable;
	}

	static bool HandleHelperCommand(ChatHandler* handler, const char* /* args */)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if(!player)
			return false;

		/*if(Pet* helper = player->CreateTamedPetFrom(HELPER,0))
		{
			helper = player->SummonPet(HELPERS::HELPER,player->GetPositionX()-2,player->GetPositionY()-2,player->GetPositionZ(),player->GetOrientation(),SUMMON_PET, 0);

			if(Creature* cHelper = helper->ToCreature())
			{
				cHelper->SetFlag(UNIT_NPC_FLAGS,UNIT_NPC_FLAG_GOSSIP);
				cHelper->SetOwnerGUID(player->GetGUID());
			}
		}*/

		return true;
	}
};

void AddSC_cs_helper()
{
	new cs_helper();
}


