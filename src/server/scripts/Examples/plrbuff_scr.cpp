/*
 * plrbuff_scr.cpp
 *
 *  Created on: Aug 15, 2014
 *      Author: dsykes
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Group.h"

class plrbuff_scr : public PlayerScript
{
public:
	plrbuff_scr() : PlayerScript("plrbuff_scr") { }

	void OnLevelChanged(Player* /*player*/, uint8 oldLevel)
	{

	}

	void OnCreatureKill(Player* player, Creature* creature)
	{
		if(!player->GetSession() || !player->IsAlive())
			return;

		uint32 cMax = creature->GetMaxHealth();
		uint32 pCur = player->GetHealth();
		uint32 pMax = player->GetMaxHealth();

		if (player->GetHealth() <= (pMax / 3))
		{
			float _strength = (player->GetStat(STAT_INTELLECT) * 3) + (creature->GetStat(STAT_INTELLECT) + (creature->getLevel() / 2));
			player->SetStat(STAT_INTELLECT, _strength);
			player->GetSession()->SendNotification(">**SURVIVOR**<");
			player->PlayDirectSound(8213,player);
			player->PlayDirectSound(8192,player);
			player->ApplyRatingMod(CR_CRIT_SPELL,_strength,true);
			player->ApplyRatingMod(CR_HIT_SPELL,_strength,true);
			player->ApplyRatingMod(CR_DODGE,_strength,true);
			player->ApplyCastTimePercentMod(512,true);
			player->SaveToDB(false);

		}
	}

	void OnPVPKill(Player* player, Player* victim)
	{
		if((!player || !player->GetSession()) || (!victim || !victim->GetSession()))
			return;

		player->PlayDirectSound(8213,player);
		player->PlayDirectSound(8192,player);

		if(Item* victimWeapon = victim->GetItemByPos(EQUIPMENT_SLOT_MAINHAND))
		{
			player->AddItem(victimWeapon->GetEntry(),1);
			player->MonsterWhisper("Victim's main weapon has been looted. ;)",player,false);

			victim->EquipNewItem(EQUIPMENT_SLOT_MAINHAND,23371,true);
		}

		player->GetSession()->SendNotification("%s killed...",victim->GetName().c_str());
	}

};

void AddSC_plrbuff_scr()
{
	new plrbuff_scr();
}


