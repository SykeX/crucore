/*
 * npc_helper.cpp
 *
 *  Created on: Aug 19, 2014
 *      Author: dsykes
 */
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Group.h"

class npc_helper : public CreatureScript
{
public:
	npc_helper() : CreatureScript("npc_helper") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		if(player->IsInCombat())
		{
			player->GetSession()->SendNotification("Your in combat.");
			return false;
		}

		if(creature->GetOwnerGUID() != player->GetGUID() && player->GetMinionGUID() != creature->GetGUID())
		{
			player->GetSession()->SendNotification("Your not the owner of this helper.");
			return false;
		}

		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "I need ya bud!", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Dismiss", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+99);
		player->PlayerTalkClass->SendGossipMenu(player->GetGossipTextId(creature), creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 actions)
	{
		player->PlayerTalkClass->ClearMenus();

		if(sender == GOSSIP_SENDER_MAIN)
		{
			switch(actions)
			{
				case GOSSIP_ACTION_INFO_DEF+1:
				{
					player->ADD_GOSSIP_ITEM_EXTENDED(0,"Greetings!, "+ player->GetName() +"! \n \n I will assist you to the best of my ability! What is it you need?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2,"",0,false);
					player->PlayerTalkClass->SendGossipMenu(907, creature->GetGUID());
				}
				break;
				case GOSSIP_ACTION_INFO_DEF+99:
				{
					//creature->SetUInt32Value(UNIT_STATE_FOLLOW,UNIT_STATE_FLEEING);
					creature->DespawnOrUnsummon(1000);
					player->SetMinionGUID(0);
				}
				break;
			}
		}
		return true;
	}
};

void AddSC_npc_helper()
{
	new npc_helper();
}




