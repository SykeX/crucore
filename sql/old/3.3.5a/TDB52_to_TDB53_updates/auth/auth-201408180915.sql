-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (i686)
--
-- Host: 25.33.63.126    Database: auth
-- ------------------------------------------------------
-- Server version	5.5.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identifier',
  `username` varchar(32) NOT NULL DEFAULT '',
  `sha_pass_hash` varchar(40) NOT NULL DEFAULT '',
  `sessionkey` varchar(80) NOT NULL DEFAULT '' COMMENT 'Temporary storage of session key used to pass data from authserver to worldserver',
  `v` varchar(64) DEFAULT NULL,
  `s` varchar(64) DEFAULT NULL,
  `reg_mail` varchar(255) NOT NULL DEFAULT '',
  `token_key` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(254) DEFAULT '',
  `joindate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `failed_logins` int(11) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `lock_country` varchar(2) NOT NULL DEFAULT '00',
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `online` tinyint(3) NOT NULL DEFAULT '0',
  `expansion` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `mutetime` bigint(40) NOT NULL DEFAULT '0',
  `mutereason` varchar(255) NOT NULL DEFAULT '',
  `muteby` varchar(50) NOT NULL DEFAULT '',
  `locale` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `os` varchar(3) NOT NULL DEFAULT '',
  `recruiter` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COMMENT='Account System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'ACCOUNTCREATION','2E51B4DC7EC35D6D3BE358391C83E6C5070A885F','',NULL,NULL,'','','','2014-04-03 06:23:26','127.0.0.1',0,0,'00','0000-00-00 00:00:00',0,2,0,'','',0,'',0),(8,'THAMIX','348E095E260AA5B86DA01FD692334A5729E8356E','','18DB569DF22AC3186BC9D180EDD830A06BFFB12F0A15C193EB83106717733AA1','FD9F427375AA460502EA3996396CECD24AB0284245C3752B38479C24248430D9','','','','2014-03-31 03:01:01','68.62.92.225',0,0,'00','2014-08-18 17:33:39',0,2,0,'','',0,'Win',0),(10,'SKUHLPHERA','7F118EC83B1FEDDF1E71A09B56402CD81A6F39D7','','380BCAB833FAAAB1BF521D13042639B5BD48138A92F3A48C9952324BFF1EEBE1','FA66D0AEEFD5BF9BF8AC4E06117C3B38B4EA24E16C80B737F5226717114186AF','','','','2014-03-31 19:33:25','66.190.208.85',0,0,'00','2014-04-03 05:52:08',0,2,0,'','',0,'Win',0),(11,'GORLOCK','DC6E984E6566BC493A9EE955537BD4986726A737','','3EC773FA82A4870C7ED674B12FB44594FA1147BFF2230DD89B9DB78CAE30A337','A80590A6C91F19C11FAFA359EA471A3E68E60F39124A6973B19AC3790AFDD38B','','','','2014-03-31 21:27:02','68.62.92.225',0,0,'00','2014-08-15 18:34:53',0,2,0,'','',0,'Win',0),(13,'VANNCAEKS','2B05DE2FB8E472B57BF1960EC7AFDC273842A23A','',NULL,NULL,'','','','2014-04-02 00:46:40','127.0.0.1',0,0,'00','0000-00-00 00:00:00',0,2,0,'','',0,'',0),(14,'TEST','3D0D99423E31FCC67A6745EC89D70D700344BC76','','2AB4B702A631FAA6985AE0A246343ECB5EF9F6F91BEB0E69DCE363236DD090EF','F4D4417CE2E8C0C95659E98611D9305B59F230805439FBA3AA0DFD208AE174D1','','','','2014-04-02 05:11:14','68.62.92.225',0,0,'00','2014-04-02 05:12:30',0,2,0,'','',0,'Win',0),(15,'WESSLEY321','12E17BAA668A1E659276D8D1611B75741681F32E','','1DAA63961BB86788F7152321F3908D79F3296540236D1AB876D3F2F5F8D05A7E','EABB5220E6F4DDB911A86AECFFB9630A0EE69FC7801D171FA908A9290EF7EB4B','','','','2014-04-03 01:07:19','24.60.188.76',0,0,'00','2014-04-11 21:01:03',0,2,0,'','',0,'Win',0),(18,'TEST3','A8D7248A12E0295913B7D1CC4DF4C9FC04CD2713','',NULL,NULL,'','','whistlerr5@comcast.net','2014-04-03 17:54:29','127.0.0.1',0,0,'00','0000-00-00 00:00:00',0,2,0,'','',0,'',0),(19,'COOKIE','CF8E6FCF0F486464F536082BEBCF2B653685120E','','73FB994C458778FD4AB808449921F2A06C5781393DEF93712AA20886C25D822E','83649097CB2AD8794A4898B7D608E8E4696E67E5BD338D6ED4133F93ACA0BB7F','','','Cookie@Cookie.me.uk','2014-04-03 17:54:44','5.64.181.123',0,0,'00','2014-08-14 01:31:45',0,2,0,'','',0,'Win',0),(20,'BINGTU','47D2C855EB4660B71F4B90A594300347F5835C65','','2F2AD1CDD220FAF95DC8E8444E0BC47C61218BB5CCC8ABFFA890693F3458BC82','E4654E4D981B0BAF0FA7ED5985411BDCFD5E057DE886298E9484F5F40C49F97F','','','bingtubod@yahoo.com','2014-04-04 21:16:55','174.63.15.204',0,0,'00','2014-07-13 22:29:34',0,2,0,'','',0,'Win',0),(21,'SLURHK321','E91E4432BB3CFD1997331E864FD8B461AEBC34B7','',NULL,NULL,'','','beefthebear@gmail.com','2014-04-05 17:19:20','127.0.0.1',0,0,'00','0000-00-00 00:00:00',0,2,0,'','',0,'',0),(22,'IMLY','B2BB55394492E074A95BB737AB7FD999CB7C8152','','54F00E0006084F756736BBD41933201D96FE8D1E6A5D55A855E3B620A02EC574','9FE6C5C1A3FC41C34153D519B643D0EC2C9935A0C35960FD20F2BA42631B0BAB','','','','2014-04-06 00:45:38','127.0.0.1',0,0,'00','2014-04-06 00:46:27',0,2,0,'','',0,'Win',0),(23,'WRATHILY','A69710F3744A536DC3ED13F99137CF05AE0AD649','','033F73F4B99ADF7930830F59783C8AC5F72F3700084191D64B1AD5C5BFF33534','8CD76BA5FFD84C9F58EA0D85C00B583AB7347F0BF6F3E8A0D43D6A1CC5597029','','','nandez707@gmail.com','2014-04-07 04:49:11','24.2.234.111',0,0,'00','2014-04-09 01:40:06',0,2,0,'','',0,'Win',0),(24,'KOUREE','A101A7DA86207C35731313326DABDDCB452A93E6','','59993E58C46DF7B953DEDBC0953A2A4848173467A746EF8760F7054E25738250','F4FF4B5CD75B9BA2E06A83F1260556A5923ADB62F623CCE114F31178F6DF4C49','','','victoriawolf91@yahoo.com','2014-04-12 21:12:35','70.141.192.47',0,0,'00','2014-04-13 21:21:50',0,2,0,'','',0,'Win',0),(25,'GOKIRA','D32B0845CD4FA81FE275928B711C1CAD81D1190A','','6220F0DC57FBB0638F4A41182BA35857E247769B003BC1EF70014374D131A730','B9D668E6975590E61D481013E952E54BB242732F0B2AD21709729793BACC218D','','','heiko_1984@yahoo.com','2014-04-17 18:52:58','174.25.64.52',0,0,'00','2014-08-13 11:14:59',0,2,0,'','',0,'Win',0),(26,'TREAZ0N','7392F17BE28CD522FCB769776B964B7ADDE2DA86','','7124BE8A2003E2AE028E857B1FEA2E574583F61C18C2BB5EFCA2FE0329DC7B47','CBEA54EF468B3DD5BA4E548122812BCA1B3F1DB20E0102DF20CD7B253E9F9553','','','BrandonsDugan@gmail.com','2014-06-06 01:19:04','173.216.122.111',0,0,'00','2014-07-11 19:28:26',0,2,0,'','',0,'Win',0),(27,'NINJAKEVIN','A24738D838023ECA78344BCAEE8B0D27297919A8','','3DA6F0CBC873A0428D76C09D03369EE022D4799663E038E15248AC3901742AC9','A86DEA9FFF70A359CBCEE0835ACB942915DD0195C9B54C4EFD51B2A8383949B1','','','kevsmokesweed2@gmail.com','2014-06-06 01:52:27','71.169.187.169',0,0,'00','2014-06-06 16:09:59',0,2,0,'','',0,'Win',0),(28,'NEXVOS','BAE51B1E55834D6A379141C745AF5657986CA8CC','','43D1892D2FA7651C563A925D86ED97EB86D973514C771DEA1F010A7595EB33CF','C710F76A4E0792FED996E08D6AF41B98982B4CFE394DF7F915CDF36A86353FA9','','','Rubenpaz02@gmail.com','2014-06-06 01:57:56','50.186.247.60',0,0,'00','2014-07-12 19:16:19',0,2,0,'','',0,'Win',0),(29,'YOUMOMMA','AEC70D0055BEE786FF98784B70F55EDDF4B036E9','','229268866C6A4FD794B2F0F9BD1B502DA80F01EA103CCB065A96986C6CE8E99B','A241CAB5495B64CA99CE5DC226BCD7017955A6405E16689A2B0F879A6B3AFFD9','','','stewie240@gmail.com','2014-06-06 19:37:10','68.62.92.225',0,0,'00','2014-08-18 19:41:58',0,2,0,'','',0,'Win',0),(30,'ANTHRACX','342581976787F6229762F1818FAEABA856B38C4F','D0C367C608D9EB7C8CEF495CA2D3275A991238FD73D9A39BEA7CC005D06CF5FCE59C285CA8E2AE60','5DBDC7C392D4FF15E4D2011F836316D8072A3893CB10E93381DB935A6E239E43','D57FF251AFE7C3E2AE5AFD37B73F9AD26F878CF83D2F978D8DDD1FAB1550BFE3','','','randyrob94@yahoo.com','2014-06-20 02:49:12','184.57.150.157',0,0,'00','2014-08-19 01:12:01',0,2,0,'','',0,'Win',0),(31,'CAPIAN','2557D1C5BACBC939C653C0136195D66C38861F25','','840A3BA609362C64BD248977B14E233E3A6FA29D65BBF235C9B69F2A805F8040','B195380A2B4BE86CBB94A319AC89530A6DF65CD11255FD77167E3A9D6DDA4233','','','m.capriogliojr@gmail.com','2014-06-21 16:40:38','96.28.76.168',0,0,'00','2014-07-01 04:15:48',0,2,0,'','',0,'Win',0),(33,'ADORA','35C54EC00005B68FC0C73FB8452DD39B191A6F05','','114A7FC9373FC1F127F00D9BC7308023517C2606184D2505FD1E2454B6E82721','E544BF9F6A467CC5AFEAE2F89C2AA0765822054601977011142B11C319E01543','','','Clipman34@gmail.com','2014-06-21 23:37:13','70.125.138.95',0,0,'00','2014-08-14 01:22:56',0,2,0,'','',0,'Win',0),(34,'DILDOZER','9A917AEFB7198A78348B1E0608FDA59D8FB2EBE3','','7C8FC964BAEAD00A36BF4F099DDC39344180168028136BBE49C9D785FDDB5588','F43A75E4226EEA466CDD3CE3D4AB19480A97FAEA83485EEBA1769BAD34AA5D01','','','drewbengal@aol.com','2014-06-24 02:11:38','65.190.131.37',0,0,'00','2014-07-09 05:21:27',0,2,0,'','',0,'Win',0),(35,'GUSTOPOE','730C28852B8FB63ABC59600B9A2CA8532D7CC0B3','','04D4B3A63AC13494A5FEFA8433D4E8FE82D9E654AD0CD3286D04A43863B1112B','99B769F0F6C1869B5A20301DE1BB6ACC22BEFF264EA7C139A8A44BF60E0029FF','','','Gustopoerocks@hotmail.com','2014-06-26 01:14:57','98.177.168.127',0,0,'00','2014-08-14 03:55:20',0,2,0,'','',0,'Win',0),(36,'HURTZ','F216921434075BCFB0B7AFA01EE18633E3CF4097','','506F54DAEB2171D970DA4368E2B0E6C071BA65213040EFFC123D1073BA6CBE48','DA53B48105019CF2404DDF0090F5CCD48946504D6422CDBB250370DF3BC64581','','','arcmagisterpyrosz@gmail.com','2014-06-30 04:09:44','174.25.113.245',0,0,'00','2014-08-19 00:07:50',0,2,0,'','',0,'Win',0),(37,'LIDRA','FAF7DF4B961D6CF8D4A13F4B68B7340DB3B6D1A6','','2A1E93CB0FF54FCF61D36CE49DD3ECC34A6882CF9701804F10DF708D3E63E06C','B61987361AA1FA1B14A82DD6F8B6F18E35ADEF9F0BD486B78C5A9EF6F994AF19','','','joithepet@gmail.com','2014-07-05 00:52:06','62.195.108.166',0,0,'00','2014-07-05 01:11:41',0,2,0,'','',0,'Win',0),(38,'LMATTHEWS89','7DB8297810A5AD2392950DD35B3BD01B82389B30','','1D83285765B86F2A08F7D27CF39B7ED6CC801E2BFFF925BCF91926FC0F6277F5','DB5B60F3295F38ED4AA266BB3C5C07F1827BB4E2C55226825008667C93DD8F09','','','Lmatthews11@hotmail.co.uk','2014-07-11 15:56:25','2.125.61.48',0,0,'00','2014-08-02 16:12:31',0,2,0,'','',0,'Win',0),(39,'COREY09','6DA8B841E6DC88672493FF3D1E5D187FE296843F','','514E2BD65F1940D2D2C715B2F6C6ABC91DA7EBBB40E3F7DE159A173719B6AC41','CC0B893D936AD95D995008281571B602730766174A744D1B13350864C693A2A3','','','jodiehill2702@gmail.com','2014-07-11 18:16:18','2.125.61.48',0,0,'00','2014-08-02 16:23:13',0,2,0,'','',0,'Win',0),(40,'RMATTHEWS90','913D676E79B88A2D848F395A80566E68C678B6FE','','2F39AA53E0AC2EA9AEA1292A898B8A931F01C0BD490BED40C97468043AC5A637','94466BC932F365C081812F18C8B9FF16926528CEE57B31C22FFD663F89D6E0E1','','','rmatthews1990@live.co.uk','2014-07-12 16:02:28','5.65.169.133',0,0,'00','2014-07-26 16:23:01',0,2,0,'','',0,'Win',0),(41,'DREYNO','1F180ABEF734C8D0DB0647AB7DC3F07C1887770E','','44FDC14AF44382787B0D4BCC4B4EA933F120403E7DC5D785B53239B7073EAE97','AA94D75CE3A644EBD6B3CC8AFFD4C7435379B0556C7541BC0303E7C90D80D755','','','neilasbloodraven@gmail.com','2014-07-15 06:20:18','72.204.93.122',0,0,'00','2014-07-15 06:23:09',0,2,0,'','',0,'Win',0),(42,'ELSO16','0527AEB7697C0BC55DDB9BD1A8E4ABB6EE293E6B','',NULL,NULL,'','','elsolopez0@gmail.com','2014-07-18 18:27:48','127.0.0.1',0,0,'00','0000-00-00 00:00:00',0,2,0,'','',0,'',0),(43,'LOKESLIGHT','88E98DB50F7FD4F0EE1660289392E75D00CC7E92','',NULL,NULL,'','','LokesLight@gmail.com','2014-07-19 03:46:41','127.0.0.1',0,0,'00','0000-00-00 00:00:00',0,2,0,'','',0,'',0),(44,'ELSO17','96C24EAD347CC7BA7EACD2CC74E9535BD49E9263','','117AD6DEFEC18E1807591CDF17F414ED304ED2B182B7525EA6ADB17CEE733EFA','9DD74A7B63D06B435060F46346459A5DD67829CB762655738142C5A4153CC559','','','elsolopez144@yahoo.com','2014-07-19 19:06:02','76.101.136.119',0,0,'00','2014-07-19 19:09:03',0,2,0,'','',0,'Win',0),(45,'THANOS','608E09DB7B015169E50E4272227A5421A474BDED','','2A0D95C293A91AEC3A3241B621A6552D1C52D891B54FF3A88F011CA2A1F65BCF','F90DD5638DA65258A098213506D416AC2575419C3EB079B4DFD3796686B62B47','','','nelsonfurtado79@gmail.com','2014-07-21 12:34:23','176.79.145.248',0,0,'00','2014-08-18 21:44:11',0,2,0,'','',0,'Win',0),(46,'THANOS2','C34157146E32CBCFA0C62CAA3282AF6409F73D0F','','40ED1B268CEBA7C7BCDF1DE2CB9C737497A3E608E1954DB0E9796CD87D77F4A9','86EDF69A11A5D7DEB0D47520A1F5C26F121D9C3210A5D7D0F1B9363F8140E8F1','','','red.eagle.new@gmail.com','2014-07-22 09:17:16','176.79.145.248',0,0,'00','2014-08-10 02:12:37',0,2,0,'','',0,'Win',0),(47,'CHOK12D','F03A46B384BC5E9C46C4ED12887959154D91BADB','',NULL,NULL,'','','riverdotcom@Hotmail.com','2014-07-22 12:40:17','127.0.0.1',0,0,'00','0000-00-00 00:00:00',0,2,0,'','',0,'',0),(48,'RDMAHONEY','779EF212320AEC2008992B17AC1771A59D46327D','','5574696C57B66A34D0FCCBA76C9608272DB9327036CD011AFFBE744B38CCFB95','8BCF8D160389CC789E917A6FDB19C6E4F213DA014039A7C7137AF1883768FB1D','','','rmahoney540@gmail.com','2014-07-22 19:39:46','68.68.70.190',0,0,'00','2014-07-22 19:41:31',0,2,0,'','',0,'Win',0),(49,'QUEENWMD','BB2EAF454828DCEC0FCD1ECAC8F8020D7778B676','','0E7FC668928D01880E1B35467276F3443554494B0B0C38516282393786453613','F20551AC4C9DF482F6B4FA512626009A8B9E156688B98E295D995384C6C2ACFF','','','marilynmichaels@gmail.com','2014-07-29 08:12:06','100.33.242.15',0,0,'00','2014-07-29 12:33:14',0,2,0,'','',0,'Win',0),(50,'RAGNOKE','1F70E5A8AEFB569FBA34486F4B24C89E31B373C4','',NULL,NULL,'','','masterNinjaWarrior1@gmail.com','2014-08-01 04:02:10','127.0.0.1',0,0,'00','0000-00-00 00:00:00',0,2,0,'','',0,'',0),(51,'OVIDELU','E984D41E097284E5764783856ACD9398158420EB','','280293E7BA9C69F67918EBF22C7353549F884C57D5B6B36B55BBDCE1ED3AA3CC','B191C2CB73E3E4B4B34A65ECA3575D3979943D69027F312561241E04973E5065','','','ovy.ovydelu@yahoo.com','2014-08-04 11:26:01','79.117.70.219',0,0,'00','2014-08-04 11:26:44',0,2,0,'','',0,'Win',0),(52,'GAFUK77','9F7ED2CF3D9936BEB4B3919640C482CA64769627','','066706DF72F45CC09D33F45092BE0D906A0C4D0162619E58AFDAD1088AED7B04','FD59070AF31B08331252C4DA265D2BD4C541211A3857DDA36CBB7DC5A858F0CF','','','kroupa378@gmail.com','2014-08-05 19:14:01','67.3.196.152',0,0,'00','2014-08-05 21:50:28',0,2,0,'','',0,'Win',0),(53,'PALADIN09','3B07E3AAA93D4539343AD3DEE1D5ECEEF987D297','','78A57AD084C566DFD56A42E32C1FE3BF6F9471D98E2C4989AFF56B3FE9C56A57','D82AE433D4328A2EA1A892C8DBF15F70F5546F58352FA50AA71B101550E49A81','','','usmcpaladin09@gmail.com','2014-08-08 12:46:00','72.200.123.54',0,0,'00','2014-08-18 20:34:19',0,2,0,'','',0,'Win',0),(54,'PRIINEL','101C3977503D91F858F24D4DBB1543147E086E5A','','0EAF1C1E9416F4C8D68B1C167E69E41A5FB39CB8C72EF2735A4A7BE0705F9D2A','A709B69DDCED67149F98C706BD6BAE1EF69855FB6A9A0179794874F9E852349D','','','prizinhaah19@gmail.com','2014-08-08 19:52:46','201.1.214.228',0,0,'00','2014-08-08 19:54:15',0,2,0,'','',0,'Win',0),(55,'TENSHUMA','EB6786DC56206988741DC55942B06DC287251557','','600A28566198CD442910AA5525F28E916A05124134251D52DA4360E18EC0F631','B64ABACB72F2C0F58C4121B9D4125FEF0EF24DB5D75F526BFB61A616876E2269','','','Countryshot69@gmail.com','2014-08-11 04:02:38','75.181.175.164',0,0,'00','2014-08-11 04:25:55',0,2,0,'','',0,'Win',0),(56,'FRISSIONX','C0730D9AEA2642035455D60195356F519909B4F8','','1C0A11ACDD80846C0D28474E53EC7A1FE87CE6166468809B1A8C411FCCBD551D','B787F0A4925CBC509D22CF3575B2323FDE709FCD5DA1B7F17F3433345F21FAE3','','','justin.wolter@gmail.com','2014-08-12 07:10:18','74.215.184.189',0,0,'00','2014-08-18 17:30:36',0,2,0,'','',0,'Win',0),(57,'ARIDS','9E585CCD75B2C6A91D2D4F4B88A6140E2DB277AF','','1F0B74E398E7B26E1F9B50C2B7F7DC96587ADDCA36EC1C9D5787D78EE311AD1D','B32FF776DEA554438339721EB2871360E3290E7F49F0DCBDF0FB545E89BE7C21','','','aridholyelite@hotmail.com','2014-08-13 00:54:49','75.181.175.164',0,0,'00','2014-08-13 00:55:49',0,2,0,'','',0,'Win',0),(58,'LOWBALL','89B387025D1B317687537B6D5CEF85E2B864A463','','35D1EC732B2D746C8D68F45471F5C76B0771EE4057EC59699F3DE27F22646D3A','ACDF391AC099EAA4FA1DDF4E0C18A8BC61E011A77B6C3DFCACEEE7A14EA9402D','','','toughguy80@hotmail.com','2014-08-14 04:33:23','74.215.67.166',0,0,'00','2014-08-18 12:47:54',0,2,0,'','',0,'Win',0),(59,'MARKER','457C1196F32225C523A1BFB920CC8AF7D0305A1E','','183AB1A30E68BFE8C8E8C6FC148D895025430CA43CF586A42F9B1498891EB31A','DEA63DB0BFF338A2D088B60E8BB5196D2DE0E084EADF6C08EF6A04A587CF7F53','','','adasd@asda.dk','2014-08-15 10:57:36','2.108.123.210',0,0,'00','2014-08-15 10:57:48',0,2,0,'','',0,'Win',0),(60,'DBLACK95','CD590463F06D896260965326295A79BD016F1049','',NULL,NULL,'','','yinnwelin95@gmail.com','2014-08-16 01:59:22','127.0.0.1',0,0,'00','0000-00-00 00:00:00',0,2,0,'','',0,'',0),(61,'DSYKES','04C9FF8BD95ED02656BCFA15B4576F94698AB649','','4012494D39C56B70E6F0F771927DD10C6E0A4ACA9B0FBAB3AF65B154CE2BEFF7','B33BA5896E50895A5ACA07B50B006BEA0690502636051DEC875270DD7E92E801','','','dezmensykes.ds@gmail.com','2014-08-17 20:53:25','108.81.217.225',0,0,'00','2014-08-18 01:48:07',0,2,0,'','',0,'Win',0);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_access`
--

DROP TABLE IF EXISTS `account_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_access` (
  `id` int(10) unsigned NOT NULL,
  `gmlevel` tinyint(3) unsigned NOT NULL,
  `RealmID` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`,`RealmID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_access`
--

LOCK TABLES `account_access` WRITE;
/*!40000 ALTER TABLE `account_access` DISABLE KEYS */;
INSERT INTO `account_access` VALUES (1,3,-1),(5,2,-1),(8,3,-1),(12,2,-1),(26,2,-1),(28,2,-1),(35,2,-1),(61,3,-1);
/*!40000 ALTER TABLE `account_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_banned`
--

DROP TABLE IF EXISTS `account_banned`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_banned` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Account ids',
  `bandate` int(10) unsigned NOT NULL DEFAULT '0',
  `unbandate` int(10) unsigned NOT NULL DEFAULT '0',
  `bannedby` varchar(50) NOT NULL,
  `banreason` varchar(255) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`bandate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ban List';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_banned`
--

LOCK TABLES `account_banned` WRITE;
/*!40000 ALTER TABLE `account_banned` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_banned` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autobroadcast`
--

DROP TABLE IF EXISTS `autobroadcast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autobroadcast` (
  `realmid` int(11) NOT NULL DEFAULT '-1',
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `weight` tinyint(3) unsigned DEFAULT '1',
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`,`realmid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autobroadcast`
--

LOCK TABLES `autobroadcast` WRITE;
/*!40000 ALTER TABLE `autobroadcast` DISABLE KEYS */;
INSERT INTO `autobroadcast` VALUES (1,1,1,'To speak to everyone, just type .nameannounce and then your message, for a list of other commands type .command'),(1,2,1,'1. Harassment on this server of all kinds is prohibited.'),(1,3,1,'2. Spawn Camping, as long as you are not in a pvp area (IE an alliance in Orgrimmar or in Warsong gultch or Gurubashi Arena) do not kill someone more than twice with in ten minutes.'),(1,4,1,'4. Hacking is not allowed, fly hacking, speed hacking, cooldown hacking etc etc. These are obvious and fairly self explanitory.'),(1,5,1,'8. English only in announce, the staff isn\'t fluant in any other language, tickets should also be written in English.'),(1,6,1,'9. Offensive language, racial slurs and sexist remarks are not allowed here, you may curse, but do not get excessive with it.'),(1,7,1,'Remember to vote, and get your friends to join, we love to see new faces after all!'),(1,8,1,'You can have up to ten proffesions, so don\'t limit yourself!~'),(1,9,1,'If you ever need any help, feel free to make a ticket, they are checked often and even if yours is not taken care of right away, it will be taken care of as soon as possible. If you for some reason can\'t leave a ticket in game do so on our forums!');
/*!40000 ALTER TABLE `autobroadcast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ip2nation`
--

DROP TABLE IF EXISTS `ip2nation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip2nation` (
  `ip` int(11) unsigned NOT NULL DEFAULT '0',
  `country` char(2) NOT NULL DEFAULT '',
  KEY `ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip2nation`
--

LOCK TABLES `ip2nation` WRITE;
/*!40000 ALTER TABLE `ip2nation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ip2nation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ip2nationcountries`
--

DROP TABLE IF EXISTS `ip2nationcountries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip2nationcountries` (
  `code` varchar(4) NOT NULL DEFAULT '',
  `iso_code_2` varchar(2) NOT NULL DEFAULT '',
  `iso_code_3` varchar(3) DEFAULT '',
  `iso_country` varchar(255) NOT NULL DEFAULT '',
  `country` varchar(255) NOT NULL DEFAULT '',
  `lat` float NOT NULL DEFAULT '0',
  `lon` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`code`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip2nationcountries`
--

LOCK TABLES `ip2nationcountries` WRITE;
/*!40000 ALTER TABLE `ip2nationcountries` DISABLE KEYS */;
/*!40000 ALTER TABLE `ip2nationcountries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ip_banned`
--

DROP TABLE IF EXISTS `ip_banned`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip_banned` (
  `ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `bandate` int(10) unsigned NOT NULL,
  `unbandate` int(10) unsigned NOT NULL,
  `bannedby` varchar(50) NOT NULL DEFAULT '[Console]',
  `banreason` varchar(255) NOT NULL DEFAULT 'no reason',
  PRIMARY KEY (`ip`,`bandate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Banned IPs';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip_banned`
--

LOCK TABLES `ip_banned` WRITE;
/*!40000 ALTER TABLE `ip_banned` DISABLE KEYS */;
/*!40000 ALTER TABLE `ip_banned` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `time` int(10) unsigned NOT NULL,
  `realm` int(10) unsigned NOT NULL,
  `type` varchar(250) DEFAULT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `string` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rbac_account_permissions`
--

DROP TABLE IF EXISTS `rbac_account_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_account_permissions` (
  `accountId` int(10) unsigned NOT NULL COMMENT 'Account id',
  `permissionId` int(10) unsigned NOT NULL COMMENT 'Permission id',
  `granted` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Granted = 1, Denied = 0',
  `realmId` int(11) NOT NULL DEFAULT '-1' COMMENT 'Realm Id, -1 means all',
  PRIMARY KEY (`accountId`,`permissionId`,`realmId`),
  KEY `fk__rbac_account_roles__rbac_permissions` (`permissionId`),
  CONSTRAINT `fk__rbac_account_permissions__account` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_account_roles__rbac_permissions` FOREIGN KEY (`permissionId`) REFERENCES `rbac_permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account-Permission relation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rbac_account_permissions`
--

LOCK TABLES `rbac_account_permissions` WRITE;
/*!40000 ALTER TABLE `rbac_account_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `rbac_account_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rbac_default_permissions`
--

DROP TABLE IF EXISTS `rbac_default_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_default_permissions` (
  `secId` int(10) unsigned NOT NULL COMMENT 'Security Level id',
  `permissionId` int(10) unsigned NOT NULL COMMENT 'permission id',
  PRIMARY KEY (`secId`,`permissionId`),
  KEY `fk__rbac_default_permissions__rbac_permissions` (`permissionId`),
  CONSTRAINT `fk__rbac_default_permissions__rbac_permissions` FOREIGN KEY (`permissionId`) REFERENCES `rbac_permissions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Default permission to assign to different account security levels';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rbac_default_permissions`
--

LOCK TABLES `rbac_default_permissions` WRITE;
/*!40000 ALTER TABLE `rbac_default_permissions` DISABLE KEYS */;
INSERT INTO `rbac_default_permissions` VALUES (0,25),(3,192),(2,193),(1,194),(0,195);
/*!40000 ALTER TABLE `rbac_default_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rbac_linked_permissions`
--

DROP TABLE IF EXISTS `rbac_linked_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_linked_permissions` (
  `id` int(10) unsigned NOT NULL COMMENT 'Permission id',
  `linkedId` int(10) unsigned NOT NULL COMMENT 'Linked Permission id',
  PRIMARY KEY (`id`,`linkedId`),
  KEY `fk__rbac_linked_permissions__rbac_permissions1` (`id`),
  KEY `fk__rbac_linked_permissions__rbac_permissions2` (`linkedId`),
  CONSTRAINT `fk__rbac_linked_permissions__rbac_permissions1` FOREIGN KEY (`id`) REFERENCES `rbac_permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_linked_permissions__rbac_permissions2` FOREIGN KEY (`linkedId`) REFERENCES `rbac_permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Permission - Linked Permission relation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rbac_linked_permissions`
--

LOCK TABLES `rbac_linked_permissions` WRITE;
/*!40000 ALTER TABLE `rbac_linked_permissions` DISABLE KEYS */;
INSERT INTO `rbac_linked_permissions` VALUES (192,21),(192,42),(192,43),(192,193),(192,196),(193,48),(193,194),(193,197),(194,1),(194,2),(194,11),(194,13),(194,14),(194,15),(194,16),(194,17),(194,18),(194,19),(194,20),(194,22),(194,23),(194,25),(194,26),(194,27),(194,28),(194,29),(194,30),(194,31),(194,32),(194,33),(194,34),(194,35),(194,36),(194,37),(194,38),(194,39),(194,40),(194,41),(194,44),(194,46),(194,47),(194,195),(194,198),(195,3),(195,4),(195,5),(195,6),(195,24),(195,49),(195,199),(196,200),(196,201),(196,202),(196,203),(196,204),(196,205),(196,206),(196,226),(196,227),(196,230),(196,231),(196,233),(196,234),(196,235),(196,238),(196,239),(196,240),(196,241),(196,242),(196,243),(196,244),(196,245),(196,246),(196,247),(196,248),(196,249),(196,250),(196,251),(196,252),(196,253),(196,254),(196,255),(196,256),(196,257),(196,258),(196,259),(196,260),(196,261),(196,262),(196,264),(196,265),(196,266),(196,267),(196,268),(196,269),(196,270),(196,271),(196,272),(196,279),(196,280),(196,283),(196,287),(196,288),(196,289),(196,290),(196,291),(196,292),(196,293),(196,294),(196,295),(196,296),(196,297),(196,298),(196,299),(196,302),(196,303),(196,304),(196,305),(196,306),(196,307),(196,308),(196,309),(196,310),(196,313),(196,314),(196,319),(196,320),(196,321),(196,322),(196,323),(196,324),(196,325),(196,326),(196,327),(196,328),(196,329),(196,330),(196,331),(196,332),(196,333),(196,334),(196,335),(196,336),(196,337),(196,338),(196,339),(196,340),(196,341),(196,342),(196,343),(196,344),(196,345),(196,346),(196,347),(196,348),(196,349),(196,350),(196,351),(196,352),(196,353),(196,354),(196,355),(196,356),(196,357),(196,358),(196,359),(196,360),(196,361),(196,362),(196,363),(196,364),(196,365),(196,366),(196,373),(196,375),(196,400),(196,401),(196,402),(196,403),(196,404),(196,405),(196,406),(196,407),(196,417),(196,418),(196,419),(196,420),(196,421),(196,422),(196,423),(196,424),(196,425),(196,426),(196,427),(196,428),(196,429),(196,434),(196,435),(196,436),(196,437),(196,438),(196,439),(196,440),(196,441),(196,442),(196,443),(196,444),(196,445),(196,446),(196,447),(196,448),(196,449),(196,450),(196,451),(196,452),(196,453),(196,454),(196,455),(196,456),(196,457),(196,458),(196,459),(196,461),(196,463),(196,464),(196,465),(196,472),(196,473),(196,474),(196,475),(196,476),(196,477),(196,478),(196,488),(196,489),(196,491),(196,492),(196,493),(196,495),(196,497),(196,498),(196,499),(196,500),(196,502),(196,503),(196,505),(196,508),(196,511),(196,513),(196,514),(196,516),(196,519),(196,522),(196,523),(196,526),(196,527),(196,529),(196,530),(196,533),(196,535),(196,536),(196,537),(196,538),(196,539),(196,540),(196,541),(196,556),(196,581),(196,582),(196,592),(196,593),(196,596),(196,602),(196,603),(196,604),(196,605),(196,606),(196,607),(196,608),(196,609),(196,610),(196,611),(196,612),(196,613),(196,615),(196,616),(196,617),(196,618),(196,619),(196,620),(196,621),(196,622),(196,623),(196,624),(196,625),(196,626),(196,627),(196,628),(196,629),(196,630),(196,633),(196,634),(196,635),(196,636),(196,637),(196,638),(196,639),(196,640),(196,641),(196,642),(196,643),(196,644),(196,645),(196,646),(196,647),(196,648),(196,649),(196,650),(196,651),(196,652),(196,653),(196,654),(196,655),(196,656),(196,657),(196,658),(196,659),(196,660),(196,661),(196,662),(196,663),(196,664),(196,665),(196,666),(196,667),(196,668),(196,669),(196,670),(196,671),(196,672),(196,673),(196,674),(196,675),(196,676),(196,677),(196,678),(196,679),(196,680),(196,681),(196,682),(196,683),(196,684),(196,685),(196,686),(196,687),(196,688),(196,689),(196,690),(196,691),(196,692),(196,693),(196,694),(196,695),(196,696),(196,697),(196,698),(196,699),(196,700),(196,701),(196,702),(196,703),(196,704),(196,705),(196,706),(196,707),(196,708),(196,709),(196,710),(196,711),(196,712),(196,713),(196,714),(196,715),(196,716),(196,717),(196,718),(196,719),(196,721),(196,722),(196,723),(196,724),(196,725),(196,726),(196,727),(196,728),(196,729),(196,730),(196,733),(196,734),(196,735),(196,736),(196,738),(196,739),(196,748),(196,753),(196,757),(196,773),(196,777),(197,232),(197,236),(197,237),(197,273),(197,274),(197,275),(197,276),(197,277),(197,284),(197,285),(197,286),(197,301),(197,311),(197,387),(197,388),(197,389),(197,390),(197,391),(197,392),(197,393),(197,394),(197,395),(197,396),(197,397),(197,398),(197,399),(197,479),(197,480),(197,481),(197,482),(197,485),(197,486),(197,487),(197,494),(197,506),(197,509),(197,510),(197,517),(197,518),(197,521),(197,542),(197,543),(197,550),(197,558),(197,568),(197,571),(197,572),(197,573),(197,574),(197,575),(197,576),(197,577),(197,578),(197,579),(197,580),(197,583),(197,584),(197,585),(197,586),(197,587),(197,588),(197,589),(197,590),(197,591),(197,594),(197,595),(197,601),(197,743),(197,750),(197,758),(197,761),(197,762),(197,763),(197,764),(197,765),(197,766),(197,767),(197,768),(197,769),(197,770),(197,771),(197,772),(197,774),(198,218),(198,300),(198,312),(198,315),(198,316),(198,317),(198,318),(198,367),(198,368),(198,369),(198,370),(198,371),(198,372),(198,374),(198,376),(198,377),(198,378),(198,379),(198,380),(198,381),(198,382),(198,383),(198,384),(198,385),(198,386),(198,408),(198,409),(198,410),(198,411),(198,412),(198,413),(198,414),(198,415),(198,416),(198,430),(198,431),(198,432),(198,433),(198,462),(198,466),(198,467),(198,468),(198,469),(198,470),(198,471),(198,483),(198,484),(198,490),(198,504),(198,512),(198,515),(198,520),(198,524),(198,528),(198,531),(198,532),(198,544),(198,545),(198,546),(198,547),(198,548),(198,549),(198,551),(198,552),(198,553),(198,554),(198,555),(198,557),(198,559),(198,560),(198,561),(198,562),(198,563),(198,564),(198,565),(198,566),(198,567),(198,569),(198,570),(198,597),(198,598),(198,599),(198,600),(198,737),(198,740),(198,741),(198,742),(198,744),(198,745),(198,746),(198,747),(198,749),(198,751),(198,752),(198,754),(198,755),(198,756),(198,759),(198,760),(199,217),(199,221),(199,222),(199,223),(199,225),(199,263),(199,496),(199,501),(199,507),(199,525),(199,534);
/*!40000 ALTER TABLE `rbac_linked_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rbac_permissions`
--

DROP TABLE IF EXISTS `rbac_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_permissions` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Permission id',
  `name` varchar(100) NOT NULL COMMENT 'Permission name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Permission List';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rbac_permissions`
--

LOCK TABLES `rbac_permissions` WRITE;
/*!40000 ALTER TABLE `rbac_permissions` DISABLE KEYS */;
INSERT INTO `rbac_permissions` VALUES (1,'Instant logout'),(2,'Skip Queue'),(3,'Join Normal Battleground'),(4,'Join Random Battleground'),(5,'Join Arenas'),(6,'Join Dungeon Finder'),(11,'Log GM trades'),(13,'Skip Instance required bosses check'),(14,'Skip character creation team mask check'),(15,'Skip character creation class mask check'),(16,'Skip character creation race mask check'),(17,'Skip character creation reserved name check'),(18,'Skip character creation heroic min level check'),(19,'Skip needed requirements to use channel check'),(20,'Skip disable map check'),(21,'Skip reset talents when used more than allowed check'),(22,'Skip spam chat check'),(23,'Skip over-speed ping check'),(24,'Two side faction characters on the same account'),(25,'Allow say chat between factions'),(26,'Allow channel chat between factions'),(27,'Two side mail interaction'),(28,'See two side who list'),(29,'Add friends of other faction'),(30,'Save character without delay with .save command'),(31,'Use params with .unstuck command'),(32,'Can be assigned tickets with .assign ticket command'),(33,'Notify if a command was not found'),(34,'Check if should appear in list using .gm ingame command'),(35,'See all security levels with who command'),(36,'Filter whispers'),(37,'Use staff badge in chat'),(38,'Resurrect with full Health Points'),(39,'Restore saved gm setting states'),(40,'Allows to add a gm to friend list'),(41,'Use Config option START_GM_LEVEL to assign new character level'),(42,'Allows to use CMSG_WORLD_TELEPORT opcode'),(43,'Allows to use CMSG_WHOIS opcode'),(44,'Receive global GM messages/texts'),(45,'Join channels without announce'),(46,'Change channel settings without being channel moderator'),(47,'Enables lower security than target check'),(48,'Enable IP, Last Login and EMail output in pinfo'),(49,'Forces to enter the email for confirmation on password change'),(50,'Allow user to check his own email with .account'),(192,'Role: Sec Level Administrator'),(193,'Role: Sec Level Gamemaster'),(194,'Role: Sec Level Moderator'),(195,'Role: Sec Level Player'),(196,'Role: Administrator Commands'),(197,'Role: Gamemaster Commands'),(198,'Role: Moderator Commands'),(199,'Role: Player Commands'),(200,'Command: .rbac'),(201,'Command: .rbac account'),(202,'Command: .rbac account permission'),(203,'Command: .rbac account permission grant'),(204,'Command: .rbac account permission deny'),(205,'Command: .rbac account permission revoke'),(206,'Command: .rbac list'),(217,'Command: .account'),(218,'Command: .account addon'),(219,'Command: .account create'),(220,'Command: .account delete'),(221,'Command: .account lock'),(222,'Command: .account lock country'),(223,'Command: .account lock ip'),(224,'Command: .account onlinelist'),(225,'Command: .account password'),(226,'Command: .account set'),(227,'Command: .account set addon'),(228,'Command: .account set gmlevel'),(229,'Command: .account set password'),(230,'Command: achievement'),(231,'Command: achievement add'),(232,'Command: arena'),(233,'Command: arena captain'),(234,'Command: arena create'),(235,'Command: arena disband'),(236,'Command: arena info'),(237,'Command: arena lookup'),(238,'Command: arena rename'),(239,'Command: ban'),(240,'Command: ban account'),(241,'Command: ban character'),(242,'Command: ban ip'),(243,'Command: ban playeraccount'),(244,'Command: baninfo'),(245,'Command: baninfo account'),(246,'Command: baninfo character'),(247,'Command: baninfo ip'),(248,'Command: banlist'),(249,'Command: banlist account'),(250,'Command: banlist character'),(251,'Command: banlist ip'),(252,'Command: unban'),(253,'Command: unban account'),(254,'Command: unban character'),(255,'Command: unban ip'),(256,'Command: unban playeraccount'),(257,'Command: bf'),(258,'Command: bf start'),(259,'Command: bf stop'),(260,'Command: bf switch'),(261,'Command: bf timer'),(262,'Command: bf enable'),(263,'Command: account email'),(264,'Command: account set sec'),(265,'Command: account set sec email'),(266,'Command: account set sec regmail'),(267,'Command: cast'),(268,'Command: cast back'),(269,'Command: cast dist'),(270,'Command: cast self'),(271,'Command: cast target'),(272,'Command: cast dest'),(273,'Command: character'),(274,'Command: character customize'),(275,'Command: character changefaction'),(276,'Command: character changerace'),(277,'Command: character deleted'),(279,'Command: character deleted list'),(280,'Command: character deleted restore'),(283,'Command: character level'),(284,'Command: character rename'),(285,'Command: character reputation'),(286,'Command: character titles'),(287,'Command: levelup'),(288,'Command: pdump'),(289,'Command: pdump load'),(290,'Command: pdump write'),(291,'Command: cheat'),(292,'Command: cheat casttime'),(293,'Command: cheat cooldown'),(294,'Command: cheat explore'),(295,'Command: cheat god'),(296,'Command: cheat power'),(297,'Command: cheat status'),(298,'Command: cheat taxi'),(299,'Command: cheat waterwalk'),(300,'Command: debug'),(301,'Command: debug anim'),(302,'Command: debug areatriggers'),(303,'Command: debug arena'),(304,'Command: debug bg'),(305,'Command: debug entervehicle'),(306,'Command: debug getitemstate'),(307,'Command: debug getitemvalue'),(308,'Command: debug getvalue'),(309,'Command: debug hostil'),(310,'Command: debug itemexpire'),(311,'Command: debug lootrecipient'),(312,'Command: debug los'),(313,'Command: debug mod32value'),(314,'Command: debug moveflags'),(315,'Command: debug play'),(316,'Command: debug play cinematics'),(317,'Command: debug play movie'),(318,'Command: debug play sound'),(319,'Command: debug send'),(320,'Command: debug send buyerror'),(321,'Command: debug send channelnotify'),(322,'Command: debug send chatmessage'),(323,'Command: debug send equiperror'),(324,'Command: debug send largepacket'),(325,'Command: debug send opcode'),(326,'Command: debug send qinvalidmsg'),(327,'Command: debug send qpartymsg'),(328,'Command: debug send sellerror'),(329,'Command: debug send setphaseshift'),(330,'Command: debug send spellfail'),(331,'Command: debug setaurastate'),(332,'Command: debug setbit'),(333,'Command: debug setitemvalue'),(334,'Command: debug setvalue'),(335,'Command: debug setvid'),(336,'Command: debug spawnvehicle'),(337,'Command: debug threat'),(338,'Command: debug update'),(339,'Command: debug uws'),(340,'Command: wpgps'),(341,'Command: deserter'),(342,'Command: deserter bg'),(343,'Command: deserter bg add'),(344,'Command: deserter bg remove'),(345,'Command: deserter instance'),(346,'Command: deserter instance add'),(347,'Command: deserter instance remove'),(348,'Command: disable'),(349,'Command: disable add'),(350,'Command: disable add achievement_criteria'),(351,'Command: disable add battleground'),(352,'Command: disable add map'),(353,'Command: disable add mmap'),(354,'Command: disable add outdoorpvp'),(355,'Command: disable add quest'),(356,'Command: disable add spell'),(357,'Command: disable add vmap'),(358,'Command: disable remove'),(359,'Command: disable remove achievement_criteria'),(360,'Command: disable remove battleground'),(361,'Command: disable remove map'),(362,'Command: disable remove mmap'),(363,'Command: disable remove outdoorpvp'),(364,'Command: disable remove quest'),(365,'Command: disable remove spell'),(366,'Command: disable remove vmap'),(367,'Command: event'),(368,'Command: event activelist'),(369,'Command: event start'),(370,'Command: event stop'),(371,'Command: gm'),(372,'Command: gm chat'),(373,'Command: gm fly'),(374,'Command: gm ingame'),(375,'Command: gm list'),(376,'Command: gm visible'),(377,'Command: go'),(378,'Command: go creature'),(379,'Command: go graveyard'),(380,'Command: go grid'),(381,'Command: go object'),(382,'Command: go taxinode'),(383,'Command: go ticket'),(384,'Command: go trigger'),(385,'Command: go xyz'),(386,'Command: go zonexy'),(387,'Command: gobject'),(388,'Command: gobject activate'),(389,'Command: gobject add'),(390,'Command: gobject add temp'),(391,'Command: gobject delete'),(392,'Command: gobject info'),(393,'Command: gobject move'),(394,'Command: gobject near'),(395,'Command: gobject set'),(396,'Command: gobject set phase'),(397,'Command: gobject set state'),(398,'Command: gobject target'),(399,'Command: gobject turn'),(400,'debug transport'),(401,'Command: guild'),(402,'Command: guild create'),(403,'Command: guild delete'),(404,'Command: guild invite'),(405,'Command: guild uninvite'),(406,'Command: guild rank'),(407,'Command: guild rename'),(408,'Command: honor'),(409,'Command: honor add'),(410,'Command: honor add kill'),(411,'Command: honor update'),(412,'Command: instance'),(413,'Command: instance listbinds'),(414,'Command: instance unbind'),(415,'Command: instance stats'),(416,'Command: instance savedata'),(417,'Command: learn'),(418,'Command: learn all'),(419,'Command: learn all my'),(420,'Command: learn all my class'),(421,'Command: learn all my pettalents'),(422,'Command: learn all my spells'),(423,'Command: learn all my talents'),(424,'Command: learn all gm'),(425,'Command: learn all crafts'),(426,'Command: learn all default'),(427,'Command: learn all lang'),(428,'Command: learn all recipes'),(429,'Command: unlearn'),(430,'Command: lfg'),(431,'Command: lfg player'),(432,'Command: lfg group'),(433,'Command: lfg queue'),(434,'Command: lfg clean'),(435,'Command: lfg options'),(436,'Command: list'),(437,'Command: list creature'),(438,'Command: list item'),(439,'Command: list object'),(440,'Command: list auras'),(441,'Command: list mail'),(442,'Command: lookup'),(443,'Command: lookup area'),(444,'Command: lookup creature'),(445,'Command: lookup event'),(446,'Command: lookup faction'),(447,'Command: lookup item'),(448,'Command: lookup itemset'),(449,'Command: lookup object'),(450,'Command: lookup quest'),(451,'Command: lookup player'),(452,'Command: lookup player ip'),(453,'Command: lookup player account'),(454,'Command: lookup player email'),(455,'Command: lookup skill'),(456,'Command: lookup spell'),(457,'Command: lookup spell id'),(458,'Command: lookup taxinode'),(459,'Command: lookup tele'),(460,'Command: lookup title'),(461,'Command: lookup map'),(462,'Command: announce'),(463,'Command: channel'),(464,'Command: channel set'),(465,'Command: channel set ownership'),(466,'Command: gmannounce'),(467,'Command: gmnameannounce'),(468,'Command: gmnotify'),(469,'Command: nameannounce'),(470,'Command: notify'),(471,'Command: whispers'),(472,'Command: group'),(473,'Command: group leader'),(474,'Command: group disband'),(475,'Command: group remove'),(476,'Command: group join'),(477,'Command: group list'),(478,'Command: group summon'),(479,'Command: pet'),(480,'Command: pet create'),(481,'Command: pet learn'),(482,'Command: pet unlearn'),(483,'Command: send'),(484,'Command: send items'),(485,'Command: send mail'),(486,'Command: send message'),(487,'Command: send money'),(488,'Command: additem'),(489,'Command: additemset'),(490,'Command: appear'),(491,'Command: aura'),(492,'Command: bank'),(493,'Command: bindsight'),(494,'Command: combatstop'),(495,'Command: cometome'),(496,'Command: commands'),(497,'Command: cooldown'),(498,'Command: damage'),(499,'Command: dev'),(500,'Command: die'),(501,'Command: dismount'),(502,'Command: distance'),(503,'Command: flusharenapoints'),(504,'Command: freeze'),(505,'Command: gps'),(506,'Command: guid'),(507,'Command: help'),(508,'Command: hidearea'),(509,'Command: itemmove'),(510,'Command: kick'),(511,'Command: linkgrave'),(512,'Command: listfreeze'),(513,'Command: maxskill'),(514,'Command: movegens'),(515,'Command: mute'),(516,'Command: neargrave'),(517,'Command: pinfo'),(518,'Command: playall'),(519,'Command: possess'),(520,'Command: recall'),(521,'Command: repairitems'),(522,'Command: respawn'),(523,'Command: revive'),(524,'Command: saveall'),(525,'Command: save'),(526,'Command: setskill'),(527,'Command: showarea'),(528,'Command: summon'),(529,'Command: unaura'),(530,'Command: unbindsight'),(531,'Command: unfreeze'),(532,'Command: unmute'),(533,'Command: unpossess'),(534,'Command: unstuck'),(535,'Command: wchange'),(536,'Command: mmap'),(537,'Command: mmap loadedtiles'),(538,'Command: mmap loc'),(539,'Command: mmap path'),(540,'Command: mmap stats'),(541,'Command: mmap testarea'),(542,'Command: morph'),(543,'Command: demorph'),(544,'Command: modify'),(545,'Command: modify arenapoints'),(546,'Command: modify bit'),(547,'Command: modify drunk'),(548,'Command: modify energy'),(549,'Command: modify faction'),(550,'Command: modify gender'),(551,'Command: modify honor'),(552,'Command: modify hp'),(553,'Command: modify mana'),(554,'Command: modify money'),(555,'Command: modify mount'),(556,'Command: modify phase'),(557,'Command: modify rage'),(558,'Command: modify reputation'),(559,'Command: modify runicpower'),(560,'Command: modify scale'),(561,'Command: modify speed'),(562,'Command: modify speed all'),(563,'Command: modify speed backwalk'),(564,'Command: modify speed fly'),(565,'Command: modify speed walk'),(566,'Command: modify speed swim'),(567,'Command: modify spell'),(568,'Command: modify standstate'),(569,'Command: modify talentpoints'),(570,'Command: npc'),(571,'Command: npc add'),(572,'Command: npc add formation'),(573,'Command: npc add item'),(574,'Command: npc add move'),(575,'Command: npc add temp'),(576,'Command: npc add delete'),(577,'Command: npc add delete item'),(578,'Command: npc add follow'),(579,'Command: npc add follow stop'),(580,'Command: npc set'),(581,'Command: npc set allowmove'),(582,'Command: npc set entry'),(583,'Command: npc set factionid'),(584,'Command: npc set flag'),(585,'Command: npc set level'),(586,'Command: npc set link'),(587,'Command: npc set model'),(588,'Command: npc set movetype'),(589,'Command: npc set phase'),(590,'Command: npc set spawndist'),(591,'Command: npc set spawntime'),(592,'Command: npc set data'),(593,'Command: npc info'),(594,'Command: npc near'),(595,'Command: npc move'),(596,'Command: npc playemote'),(597,'Command: npc say'),(598,'Command: npc textemote'),(599,'Command: npc whisper'),(600,'Command: npc yell'),(601,'Command: npc tame'),(602,'Command: quest'),(603,'Command: quest add'),(604,'Command: quest complete'),(605,'Command: quest remove'),(606,'Command: quest reward'),(607,'Command: reload'),(608,'Command: reload access_requirement'),(609,'Command: reload achievement_criteria_data'),(610,'Command: reload achievement_reward'),(611,'Command: reload all'),(612,'Command: reload all achievement'),(613,'Command: reload all area'),(615,'Command: reload all gossips'),(616,'Command: reload all item'),(617,'Command: reload all locales'),(618,'Command: reload all loot'),(619,'Command: reload all npc'),(620,'Command: reload all quest'),(621,'Command: reload all scripts'),(622,'Command: reload all spell'),(623,'Command: reload areatrigger_involvedrelation'),(624,'Command: reload areatrigger_tavern'),(625,'Command: reload areatrigger_teleport'),(626,'Command: reload auctions'),(627,'Command: reload autobroadcast'),(628,'Command: reload command'),(629,'Command: reload conditions'),(630,'Command: reload config'),(633,'Command: reload creature_ai_texts'),(634,'Command: reload creature_questender'),(635,'Command: reload creature_linked_respawn'),(636,'Command: reload creature_loot_template'),(637,'Command: reload creature_onkill_reputation'),(638,'Command: reload creature_queststarter'),(639,'Command: reload creature_summon_groups'),(640,'Command: reload creature_template'),(641,'Command: reload disables'),(642,'Command: reload disenchant_loot_template'),(643,'Command: reload event_scripts'),(644,'Command: reload fishing_loot_template'),(645,'Command: reload game_graveyard_zone'),(646,'Command: reload game_tele'),(647,'Command: reload gameobject_questender'),(648,'Command: reload gameobject_loot_template'),(649,'Command: reload gameobject_queststarter'),(650,'Command: reload gm_tickets'),(651,'Command: reload gossip_menu'),(652,'Command: reload gossip_menu_option'),(653,'Command: reload item_enchantment_template'),(654,'Command: reload item_loot_template'),(655,'Command: reload item_set_names'),(656,'Command: reload lfg_dungeon_rewards'),(657,'Command: reload locales_achievement_reward'),(658,'Command: reload locales_creature'),(659,'Command: reload locales_creature_text'),(660,'Command: reload locales_gameobject'),(661,'Command: reload locales_gossip_menu_option'),(662,'Command: reload locales_item'),(663,'Command: reload locales_item_set_name'),(664,'Command: reload locales_npc_text'),(665,'Command: reload locales_page_text'),(666,'Command: reload locales_points_of_interest'),(667,'Command: reload locales_quest'),(668,'Command: reload mail_level_reward'),(669,'Command: reload mail_loot_template'),(670,'Command: reload milling_loot_template'),(671,'Command: reload npc_spellclick_spells'),(672,'Command: reload npc_trainer'),(673,'Command: reload npc_vendor'),(674,'Command: reload page_text'),(675,'Command: reload pickpocketing_loot_template'),(676,'Command: reload points_of_interest'),(677,'Command: reload prospecting_loot_template'),(678,'Command: reload quest_poi'),(679,'Command: reload quest_template'),(680,'Command: reload rbac'),(681,'Command: reload reference_loot_template'),(682,'Command: reload reserved_name'),(683,'Command: reload reputation_reward_rate'),(684,'Command: reload reputation_spillover_template'),(685,'Command: reload skill_discovery_template'),(686,'Command: reload skill_extra_item_template'),(687,'Command: reload skill_fishing_base_level'),(688,'Command: reload skinning_loot_template'),(689,'Command: reload smart_scripts'),(690,'Command: reload spell_required'),(691,'Command: reload spell_area'),(692,'Command: reload spell_bonus_data'),(693,'Command: reload spell_group'),(694,'Command: reload spell_learn_spell'),(695,'Command: reload spell_loot_template'),(696,'Command: reload spell_linked_spell'),(697,'Command: reload spell_pet_auras'),(698,'Command: reload spell_proc_event'),(699,'Command: reload spell_proc'),(700,'Command: reload spell_scripts'),(701,'Command: reload spell_target_position'),(702,'Command: reload spell_threats'),(703,'Command: reload spell_group_stack_rules'),(704,'Command: reload trinity_string'),(705,'Command: reload warden_action'),(706,'Command: reload waypoint_scripts'),(707,'Command: reload waypoint_data'),(708,'Command: reload vehicle_accessory'),(709,'Command: reload vehicle_template_accessory'),(710,'Command: reset'),(711,'Command: reset achievements'),(712,'Command: reset honor'),(713,'Command: reset level'),(714,'Command: reset spells'),(715,'Command: reset stats'),(716,'Command: reset talents'),(717,'Command: reset all'),(718,'Command: server'),(719,'Command: server corpses'),(720,'Command: server exit'),(721,'Command: server idlerestart'),(722,'Command: server idlerestart cancel'),(723,'Command: server idleshutdown'),(724,'Command: server idleshutdown cancel'),(725,'Command: server info'),(726,'Command: server plimit'),(727,'Command: server restart'),(728,'Command: server restart cancel'),(729,'Command: server set'),(730,'Command: server set closed'),(731,'Command: server set difftime'),(732,'Command: server set loglevel'),(733,'Command: server set motd'),(734,'Command: server shutdown'),(735,'Command: server shutdown cancel'),(736,'Command: server motd'),(737,'Command: tele'),(738,'Command: tele add'),(739,'Command: tele del'),(740,'Command: tele name'),(741,'Command: tele group'),(742,'Command: ticket'),(743,'Command: ticket assign'),(744,'Command: ticket close'),(745,'Command: ticket closedlist'),(746,'Command: ticket comment'),(747,'Command: ticket complete'),(748,'Command: ticket delete'),(749,'Command: ticket escalate'),(750,'Command: ticket escalatedlist'),(751,'Command: ticket list'),(752,'Command: ticket onlinelist'),(753,'Command: ticket reset'),(754,'Command: ticket response'),(755,'Command: ticket response append'),(756,'Command: ticket response appendln'),(757,'Command: ticket togglesystem'),(758,'Command: ticket unassign'),(759,'Command: ticket viewid'),(760,'Command: ticket viewname'),(761,'Command: titles'),(762,'Command: titles add'),(763,'Command: titles current'),(764,'Command: titles remove'),(765,'Command: titles set'),(766,'Command: titles set mask'),(767,'Command: wp'),(768,'Command: wp add'),(769,'Command: wp event'),(770,'Command: wp load'),(771,'Command: wp modify'),(772,'Command: wp unload'),(773,'Command: wp reload'),(774,'Command: wp show'),(777,'Command: mailbox');
/*!40000 ALTER TABLE `rbac_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realmcharacters`
--

DROP TABLE IF EXISTS `realmcharacters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `realmcharacters` (
  `realmid` int(10) unsigned NOT NULL DEFAULT '0',
  `acctid` int(10) unsigned NOT NULL,
  `numchars` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`realmid`,`acctid`),
  KEY `acctid` (`acctid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Realm Character Tracker';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realmcharacters`
--

LOCK TABLES `realmcharacters` WRITE;
/*!40000 ALTER TABLE `realmcharacters` DISABLE KEYS */;
INSERT INTO `realmcharacters` VALUES (1,1,0),(1,5,4),(1,6,0),(1,7,0),(1,8,10),(1,9,3),(1,10,3),(1,11,7),(1,12,6),(1,13,0),(1,14,1),(1,15,3),(1,16,0),(1,17,0),(1,18,0),(1,19,4),(1,20,1),(1,21,0),(1,22,1),(1,23,2),(1,24,1),(1,25,1),(1,26,1),(1,27,1),(1,28,1),(1,29,5),(1,30,5),(1,31,8),(1,32,0),(1,33,1),(1,34,1),(1,35,6),(1,36,5),(1,37,1),(1,38,2),(1,39,8),(1,40,3),(1,41,1),(1,42,0),(1,43,0),(1,44,0),(1,45,2),(1,46,2),(1,47,0),(1,48,1),(1,49,1),(1,50,0),(1,51,1),(1,52,1),(1,53,2),(1,54,1),(1,55,1),(1,56,2),(1,57,1),(1,58,1),(1,59,1),(1,60,0),(1,61,1);
/*!40000 ALTER TABLE `realmcharacters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realmlist`
--

DROP TABLE IF EXISTS `realmlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `realmlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '127.0.0.1',
  `localAddress` varchar(255) NOT NULL DEFAULT '127.0.0.1',
  `localSubnetMask` varchar(255) NOT NULL DEFAULT '255.255.255.0',
  `port` smallint(5) unsigned NOT NULL DEFAULT '8085',
  `icon` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `flag` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `timezone` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `allowedSecurityLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `population` float unsigned NOT NULL DEFAULT '0',
  `gamebuild` int(10) unsigned NOT NULL DEFAULT '12340',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Realm System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realmlist`
--

LOCK TABLES `realmlist` WRITE;
/*!40000 ALTER TABLE `realmlist` DISABLE KEYS */;
INSERT INTO `realmlist` VALUES (1,'Project Crucible','68.62.92.225','127.0.0.1','255.255.255.0',8085,0,1,1,0,0,12340);
/*!40000 ALTER TABLE `realmlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uptime`
--

DROP TABLE IF EXISTS `uptime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uptime` (
  `realmid` int(10) unsigned NOT NULL,
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `uptime` int(10) unsigned NOT NULL DEFAULT '0',
  `maxplayers` smallint(5) unsigned NOT NULL DEFAULT '0',
  `revision` varchar(255) NOT NULL DEFAULT 'Trinitycore',
  PRIMARY KEY (`realmid`,`starttime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Uptime system';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uptime`
--

LOCK TABLES `uptime` WRITE;
/*!40000 ALTER TABLE `uptime` DISABLE KEYS */;
INSERT INTO `uptime` VALUES (1,1396231396,600,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396232380,1201,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396234167,601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396235273,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396236401,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396236590,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396236714,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396236958,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396237525,600,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396238633,8400,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396247441,3001,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396282310,13201,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396296086,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396296525,9000,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396305635,24600,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396330443,9000,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396363916,9001,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396373439,3602,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396380448,600,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396381492,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396381733,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396382009,4200,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396392159,1200,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396393874,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396394036,1800,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396396350,1201,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396399587,1801,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396401801,9600,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396411560,3000,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396414828,600,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396415545,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396415737,2400,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396418737,1800,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396422358,1800,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396447643,28800,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396476798,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396476999,23401,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396500956,601,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396501609,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396501943,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396502081,600,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396502945,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396503121,52200,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396555613,10201,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396566080,2401,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396568882,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396569021,10800,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396580267,601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396581027,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396581217,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396581414,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396581516,10800,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396628361,16201,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396644708,16201,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396660966,600,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396661668,1200,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396663024,81600,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396745176,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396745294,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396745445,10200,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396755771,3001,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396759117,146833,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396906177,34801,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396961566,22202,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396988116,2401,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396990962,3601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1396994938,19201,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397014554,56401,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397071276,601,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397072045,10800,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397083259,601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397084166,1800,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397086239,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397086426,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397086543,26400,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397113776,52201,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397166363,9601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397177325,1201,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397180205,69601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397255180,89401,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397349193,104834,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397455271,601,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397485665,7800,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397494541,60003,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397578806,15601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397594830,7200,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1397760729,88201,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1400694856,1801,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1400978184,3001,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1401063208,1802,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402017488,1201,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402019273,1201,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402020604,2400,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402025697,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402026354,8400,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402070290,4800,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402076959,1201,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402081113,9000,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402090558,7200,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402098372,7201,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402150482,6601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402179339,1801,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402181610,1801,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402183678,601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402184567,1801,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402187272,1800,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1402509522,46801,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403213720,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403214287,4800,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403219508,63000,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403282564,46801,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403329965,38402,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403368629,18001,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403387138,73201,6,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403460954,105601,6,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403566683,16201,6,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403583442,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403583639,49200,5,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403633369,15601,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403649326,15001,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403664631,1801,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403666582,50401,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403717427,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403717631,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403717772,37801,6,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403756131,159601,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403915987,601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403917017,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403917330,600,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403918023,1800,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403920373,600,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403921502,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403921683,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403922187,1800,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403924587,4800,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403929828,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403929978,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403930294,6600,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403937316,600,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403937970,600,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403939132,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403939723,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403939991,1200,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403941227,57000,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1403998342,58800,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404057728,36000,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404100993,5403,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404106917,1203,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404108292,52571,6,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404160976,75408,5,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404236878,16203,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404253326,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404253889,69051,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404323121,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404325601,255601,5,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404581867,5401,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404587810,162606,6,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404752149,1203,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404754924,1202,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404756337,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404756559,91675,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404849907,18601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404872383,51003,8,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1404923730,88201,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405012299,5401,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405017810,73801,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405092707,13584,5,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405106374,2402,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405109349,159600,5,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405269412,264600,5,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405534373,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405534577,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405534649,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405534722,1800,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405536822,163201,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405700344,56860,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405776906,55509,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1405832500,382801,5,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406215937,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406216085,88800,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406305204,169633,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406475454,9603,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406485638,7483,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406493210,7050,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406500385,6001,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406506843,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406507353,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406507786,0,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406507902,31756,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406545728,26270,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406572104,10610,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406582840,10582,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406594968,34371,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406643364,7491,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406651854,3601,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406656846,42002,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1406699417,342581,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1407044719,43203,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1407088091,114601,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1407203262,57602,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1407261438,385362,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1407692742,17402,2,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1407723276,98403,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1407822001,39601,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1407861733,124201,6,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1407987816,99003,5,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1408087639,158403,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1408246478,72436,4,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1408320508,62488,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1408383073,10662,3,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1408394444,3604,1,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)'),(1,1408408646,603,0,'TrinityCore rev. d5d28057710e 2014-02-27 21:03:57 +0100 (master branch) (Win32, Release)');
/*!40000 ALTER TABLE `uptime` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-18 21:16:04
